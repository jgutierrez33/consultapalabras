/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.retirofondos.dao;

import com.mycompany.retirofondos.dao.exceptions.NonexistentEntityException;
import com.mycompany.retirofondos.dao.exceptions.PreexistingEntityException;
import com.mycompany.retirofondos.entety.Inscripcionretiro;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jgutierrez
 */
public class InscripcionretiroJpaController implements Serializable {

    public InscripcionretiroJpaController() {
        
    }
    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("clienteDS");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Inscripcionretiro inscripcionretiro) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(inscripcionretiro);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findInscripcionretiro(inscripcionretiro.getRut()) != null) {
                throw new PreexistingEntityException("Inscripcionretiro " + inscripcionretiro + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Inscripcionretiro inscripcionretiro) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            inscripcionretiro = em.merge(inscripcionretiro);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = inscripcionretiro.getRut();
                if (findInscripcionretiro(id) == null) {
                    throw new NonexistentEntityException("The inscripcionretiro with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Inscripcionretiro inscripcionretiro;
            try {
                inscripcionretiro = em.getReference(Inscripcionretiro.class, id);
                inscripcionretiro.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The inscripcionretiro with id " + id + " no longer exists.", enfe);
            }
            em.remove(inscripcionretiro);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Inscripcionretiro> findInscripcionretiroEntities() {
        return findInscripcionretiroEntities(true, -1, -1);
    }

    public List<Inscripcionretiro> findInscripcionretiroEntities(int maxResults, int firstResult) {
        return findInscripcionretiroEntities(false, maxResults, firstResult);
    }

    private List<Inscripcionretiro> findInscripcionretiroEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Inscripcionretiro.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Inscripcionretiro findInscripcionretiro(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Inscripcionretiro.class, id);
        } finally {
            em.close();
        }
    }

    public int getInscripcionretiroCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Inscripcionretiro> rt = cq.from(Inscripcionretiro.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
